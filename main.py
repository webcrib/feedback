import webapp2, jinja2, os
from webapp2_extras import routes
from models import User, PasswordResetToken, Company, Subscriber, Feedback
from functions import *
import json as simplejson
import logging
import urllib
import time
import uuid
import datetime
import hashlib
import base64
import facebook
import csv
import random
import string
import hmac

from google.appengine.api import urlfetch
from google.appengine.ext import ndb
from google.appengine.api import memcache

from settings import SETTINGS
from settings import SECRET_SETTINGS

jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)), autoescape=True)


def login_required(fn):
    '''So we can decorate any RequestHandler with #@login_required'''
    def wrapper(self, *args):
        if not self.user:
            self.redirect(self.uri_for('www-login', referred=self.request.path))
        else:
            return fn(self, *args)
    return wrapper


def admin_required(fn):
    '''So we can decorate any RequestHandler with @admin_required'''
    def wrapper(self, *args):
        if not self.user:
            self.redirect(self.uri_for('www-login'))
        elif self.user.admin:
            return fn(self, *args)
        else:
            self.redirect(self.uri_for('www-front'))
    return wrapper


def hash_password(username, password):
    i = username + password + SECRET_SETTINGS["password_salt"]
    return base64.b64encode(hashlib.sha1(i).digest())


"""Request Handlers Start Here"""


# post (ADDING)
def create_company(company_name, company_address, company_phone):
    if company_name:
        company = Company()
        company.company_name = company_name
        company.company_address = company_address
        company.company_phone = company_phone
        company.put()
    return True

def create_subscriber(company_key, subscriber_name, subscriber_address, subscriber_phone):
    if company_key:
        company_key = ndb.Key(urlsafe=company_key)
        subscriber = Subscriber()
        subscriber.branch_name = subscriber_name
        subscriber.branch_address = subscriber_address
        subscriber.branch_phone = subscriber_phone
        subscriber.company = company_key
        subscriber.put()
    return True


# post (UPDATE)
def update_company(key, company_name, company_address, company_phone):
    if key:
        company = ndb.Key(urlsafe=key).get()
        if company:
            if company_name:
                company.company_name = company_name
            if company_address:
                company.company_address = company_address
            if company_phone:
                company.company_phone = company_phone
            company.put()
    return True

def update_subscriber(key, subscriber_name, subscriber_address, subscriber_phone):
    if key:
        subscriber = ndb.Key(urlsafe=key).get()
        if subscriber:
            if subscriber_name:
                subscriber.branch_name = subscriber_name
            if subscriber_address:
                subscriber.branch_address = subscriber_address
            if subscriber_phone:
                subscriber.branch_phone = subscriber_phone
            subscriber.put()
    return True


# get (READ)
def get_company():
    companies = Company.query().fetch(30)
    return companies

def get_subscriber():
    subscribers = Subscriber.query().fetch(30)
    return subscribers

def get_feedback():
    feedbacks = Feedback.query().order(-Feedback.created).fetch(30)
    return feedbacks

def get_specific_company(key):
    if key:
        company = ndb.Key(urlsafe=key).get()
        return company


def get_specific_subscriber(key):
    if key:
        subscriber = ndb.Key(urlsafe=key).get()
        return subscriber

# get (DELETE)
def delete_company(key):
    if key:
        company = ndb.Key(urlsafe=key).get()
        company.key.delete()
        return company

def delete_subscriber(key):
    if key:
        subscriber = ndb.Key(urlsafe=key).get()
        subscriber.key.delete()
        return subscriber


class BaseHandler(webapp2.RequestHandler):
    def __init__(self, request=None, response=None):
        self.now = datetime.datetime.now()
        self.tv = {}
        self.settings = SETTINGS.copy()
        self.initialize(request, response)
        self.has_pass = False
        self.tv["version"] = os.environ['CURRENT_VERSION_ID']
        self.local = False
        if "127.0.0.1" in self.request.uri or "localhost" in self.request.uri:
            self.local = True
        # misc
        self.tv["current_url"] = self.request.uri

        if "?" in self.request.uri:
            self.tv["current_base_url"] = self.request.uri[0:(self.request.uri.find('?'))]
        else:
            self.tv["current_base_url"] = self.request.uri

        try:
            self.tv["safe_current_base_url"] = urllib.quote(self.tv["current_base_url"])
        except:
            logging.exception("safe url error")

        self.tv["request_method"] = self.request.method

        self.session = self.get_session()
        self.user = self.get_current_user()


    def render(self, template_path=None, force=False):
        self.tv["current_timestamp"] = time.mktime(self.now.timetuple())
        self.settings["current_year"] = self.now.year
        self.tv["settings"] = self.settings

        if self.request.get('error'):
            self.tv["error"] = self.request.get("error").strip()
        if self.request.get('success'):
            self.tv["success"] = self.request.get("success").strip()
        if self.request.get('warning'):
            self.tv["warning"] = self.request.get("warning").strip()

        if self.user:
            self.tv["user"] = self.user.to_object()

        if self.request.get('json') or not template_path:
            self.response.out.write(simplejson.dumps(self.tv))
            return

        template = jinja_environment.get_template(template_path)
        self.response.out.write(template.render(self.tv))
        logging.debug(self.tv)


    def get_session(self):
        from gaesessions import get_current_session
        return get_current_session()


    def get_current_user(self):
        if self.session.has_key("user"):
            user = User.get_by_id(self.session["user"])
            return user
        else:
            return None


    def login(self, user):
        self.session["user"] = user.key.id()
        return


    def logout(self):
        if self.session.is_active():
            self.session.terminate()
            return


    def iptolocation(self):
        country = self.request.headers.get('X-AppEngine-Country')
        logging.info("COUNTRY: " + str(country))
        if country == "GB":
            country = "UK"
        if country == "ZZ":
            country = ""
        if country is None:
            country = ""
        return country



class FrontPage(BaseHandler):
    def get(self):
        if self.user:
            self.redirect(self.uri_for('www-dashboard'))
            return

        self.redirect(self.uri_for('www-login'))


class RegisterPage(BaseHandler):
    def get(self):
        if self.user:
            self.redirect(self.uri_for('www-dashboard', referred="register"))
            return

        self.tv["current_page"] = "REGISTER"
        self.render('frontend/register.html')


    def post(self):
        if self.user:
            self.redirect(self.uri_for('www-dashboard'))
            return

        if self.request.get('password') and self.request.get('name'):
            username = self.request.get('name').strip()
            password = self.request.get('password')
            user = User.get_by_id(username)
            if user:
                self.redirect(self.uri_for('www-login', error = "User already exists. Please log in."))
                return
            user = User(id=username)
            user.password = hash_password(username, password)
            user.name = username
            user.put()
            self.login(user)
            if self.request.get('goto'):
                self.redirect(self.request.get('goto'))
            else:
                self.redirect(self.uri_for('www-dashboard'))
            return
        else:
            self.redirect(self.uri_for('www-register', error = "Please enter all the information required."))


class Logout(BaseHandler):
    def get(self):
        if self.user:
            self.logout()
        self.redirect(self.uri_for('www-login', referred="logout"))


class LoginPage(BaseHandler):
    def get(self):
        if self.user:
            self.redirect(self.uri_for('www-dashboard', referred="login"))
            return

        if self.request.get('username'):
            self.tv["username"] = self.request.get("username").strip()

        self.tv["current_page"] = "LOGIN"
        self.render('frontend/login.html')


    def post(self):
        if self.user:
            self.redirect(self.uri_for('www-dashboard'))
            return

        if self.request.get('username') and self.request.get('password'):
            username = self.request.get('username').strip().lower()
            password = self.request.get('password')
            user = User.get_by_id(username)
            if not user:
                self.redirect(self.uri_for('www-login', error="User not found. Please try another username or contact the admin to register."))
                return
            if user.password == hash_password(username, password):
                self.login(user)
                if self.request.get('goto'):
                    self.redirect(self.request.get('goto'))
                else:
                    self.redirect(self.uri_for('www-dashboard'))
                return
            else:
                self.redirect(self.uri_for('www-login', error="Wrong password. Please try again.", username=username))
                return
        else:
            self.redirect(self.uri_for('www-login', error="Please enter your username and password."))

class DashboardPage(BaseHandler):
    @login_required
    def get(self):
        self.tv["current_page"] = "DASHBOARD"

        try:
            start = datetime.datetime.strptime(self.request.get('start'), '%Y-%m-%d')
            self.tv['start'] = start.strftime("%b %d, %Y")
            self.tv['start_input'] = self.request.get('start')
        except:
            start = (datetime.datetime.now() + datetime.timedelta(hours=8)).date() - datetime.timedelta(days = 6)
            self.tv['start'] = start.strftime("%b %d, %Y")

        try:
            end_raw = datetime.datetime.strptime(self.request.get('end'), '%Y-%m-%d')
            end = end_raw + datetime.timedelta(hours=23, minutes=59, seconds=59)
            self.tv['end'] = end.strftime("%b %d, %Y")
            self.tv['end_input'] = self.request.get('end')

        except:
            ends = (datetime.datetime.now() + datetime.timedelta(hours=8)).strftime('%Y-%m-%d')
            end_raw = datetime.datetime.strptime(ends, '%Y-%m-%d')
            end = (datetime.datetime.now() + datetime.timedelta(hours=8)).date() + datetime.timedelta(hours=23, minutes=59, seconds=59)
            self.tv['end'] = end.strftime("%b %d, %Y")

        if start:
            start = datetime.datetime(start.year, start.month, start.day, 00, 00, 00) - datetime.timedelta(hours=8)
            query = Feedback.query(Feedback.created >= start)
            if query:
                self.tv['start_date_diff'] = (datetime.datetime.now() - start).days

        if end:
            end = datetime.datetime(end.year, end.month, end.day, 23, 59, 59) - datetime.timedelta(hours=8)
            query = query.filter(Feedback.created <= end)
            self.tv['end_date_diff'] = (datetime.datetime.now() - end_raw).days

        query = query.order(-Feedback.created)

        self.render('frontend/dashboard.html')

    @login_required
    def post(self):

        self.redirect(self.request.referer)

class GraphsPage(BaseHandler):
    @login_required
    def get(self):
        self.tv["line-stat"] = []

        try:
            start = datetime.datetime.strptime(self.request.get('start'), '%Y-%m-%d')
            self.tv['start'] = start.strftime("%b %d, %Y")
            self.tv['start_input'] = self.request.get('start')
        except:
            start = datetime.datetime.now().date() - datetime.timedelta(days = 6)
            self.tv['start'] = start.strftime("%b %d, %Y")

        try:
            end_raw = datetime.datetime.strptime(self.request.get('end'), '%Y-%m-%d')
            end = end_raw + datetime.timedelta(hours=23, minutes=59, seconds=59)
            self.tv['end'] = end.strftime("%b %d, %Y")
            self.tv['end_input'] = self.request.get('end')

        except:
            ends = (datetime.datetime.now()).strftime('%Y-%m-%d')
            end_raw = datetime.datetime.strptime(ends, '%Y-%m-%d')
            end = datetime.datetime.now().date() + datetime.timedelta(hours=23, minutes=59, seconds=59)
            self.tv['end'] = end.strftime("%b %d, %Y")

        if start:
            start = datetime.datetime(start.year, start.month, start.day, 00, 00, 00)
            query = Feedback.query(Feedback.created >= start)
            if query:
                self.tv['start_date_diff'] = ((datetime.datetime.now() + datetime.timedelta(hours=8)) - start).days

        if end:
            end = datetime.datetime(end.year, end.month, end.day, 23, 59, 59)
            query = query.filter(Feedback.created <= end)
            self.tv['end_date_diff'] = ((datetime.datetime.now() + datetime.timedelta(hours=8)) - end_raw).days
            logging.critical(((datetime.datetime.now() + datetime.timedelta(hours=8)) - end_raw).days)

        self.render('frontend/graph.html')


class CompaniesPage(BaseHandler):
    @login_required
    def get(self):
        self.tv["current_page"] = "DASHBOARD"

        companies = get_company()
        self.tv['companies'] = []
        for company in companies:
            self.tv['companies'].append(company.to_object())

        self.render('frontend/companies.html')

    @login_required
    def post(self):
        if self.request.get('save_company'):
            create_company(self.request.get('company_name').strip(), self.request.get('company_address').strip(), self.request.get('company_phone').strip())



        self.redirect(self.request.referer)


class CompanyUpdate(BaseHandler):
    @login_required
    def get(self, key):
        company = get_specific_company(key)
        if company:
            self.tv['company'] = company.to_object()

        self.render('frontend/company_edit.html')

    @login_required
    def post(self, *args, **kwargs):
        if self.request.get('edit_company'):
            update_company(self.request.get('company_key'), self.request.get('company_name').strip(), self.request.get('company_address').strip(), self.request.get('company_phone').strip())

        self.redirect(self.uri_for('www-company'))

class CompanyDelete(BaseHandler):
    @login_required
    def get(self, key):
        company = get_specific_company(key)
        if company:
            self.tv['company'] = company.to_object()

        self.render('frontend/company_delete.html')

    @login_required
    def post(self, *args, **kwargs):
        if self.request.get('delete_company'):
            delete_company(self.request.get('company_key'))

        self.redirect(self.uri_for('www-company'))


class SubscribersPage(BaseHandler):
    @login_required
    def get(self):
        companies = get_company()
        self.tv['companies'] = []
        for company in companies:
            self.tv['companies'].append(company.to_object())

        subscribers = get_subscriber()
        self.tv['subscribers'] = []
        for subscriber in subscribers:
            self.tv['subscribers'].append(subscriber.to_object())

        self.render('frontend/subscribers.html')

    @login_required
    def post(self):
        if self.request.get('save_subscriber'):
            create_subscriber(self.request.get('company_name'), self.request.get('subscriber_name').strip(), self.request.get('subscriber_address').strip(), self.request.get('subscriber_phone').strip())

        self.redirect(self.request.referer)

class SubscriberUpdate(BaseHandler):
    @login_required
    def get(self, key):
        subscriber = get_specific_subscriber(key)
        if subscriber:
            self.tv['subscriber'] = subscriber.to_object()

        self.render('frontend/subscriber_edit.html')

    @login_required
    def post(self, *args, **kwargs):
        if self.request.get('edit_subscriber'):
            update_subscriber(self.request.get('subscriber_key'), self.request.get('subscriber_name').strip(), self.request.get('subscriber_address').strip(), self.request.get('subscriber_phone').strip())

        self.redirect(self.uri_for('www-subscribers'))

class SubscriberDelete(BaseHandler):
    @login_required
    def get(self, key):
        subscriber = get_specific_subscriber(key)
        if subscriber:
            self.tv['subscriber'] = subscriber.to_object()

        self.render('frontend/subscriber_delete.html')

    @login_required
    def post(self, *args, **kwargs):
        if self.request.get('delete_subscriber'):
            delete_subscriber(self.request.get('subscriber_key'))

        self.redirect(self.uri_for('www-subscribers'))

class FeedbacksPage(BaseHandler):
    @login_required
    def get(self):
        companies = get_company()
        self.tv['companies'] = []
        for company in companies:
            self.tv['companies'].append(company.to_object())

        subscribers = get_subscriber()
        self.tv['subscribers'] = []
        for subscriber in subscribers:
            self.tv['subscribers'].append(subscriber.to_object())

        feedbacks = get_feedback()
        self.tv['feedbacks'] = []
        for feedback in feedbacks:
            self.tv['feedbacks'].append(feedback.to_object())

        self.render('frontend/feedbacks.html')

    @login_required
    def post(self):
        feedback = Feedback()
        if self.request.get('company_name'):
            company = get_specific_company(self.request.get('company_name'))
            feedback.company_name = company.company_name
            feedback.company_key = company.key
        if self.request.get('branch_name'):
            branch = get_specific_subscriber(self.request.get('branch_name'))
            feedback.branch_name = branch.branch_name
            feedback.branch_phone = branch.branch_phone
            feedback.branch_key = branch.key
        feedback.feedback_type = self.request.get('feedback_type')
        feedback.feedback_details = self.request.get('feedback_details')
        feedback.put()

        self.redirect(self.request.referer)

class FeedbacksGraphPage(BaseHandler):
    @login_required
    def get(self):
        self.tv['feedback'] = []

        details = {}
        details["feedback_list"] = []
        results = None
        n = 300
        more = True
        new_cursor = None
        more2 = True

        while more2:
            if new_cursor:
                results, cursor, more = Feedback.query().order(-Feedback.created).fetch_page(n, start_cursor = new_cursor)
            else:
                results, cursor, more = Feedback.query().order(-Feedback.created).fetch_page(n)

            if cursor and more:
                self.tv['cursor'] = cursor.urlsafe()

            new_cursor = cursor
            if results:
                for result in results:
                    details["feedback_list"].append(result.to_object())

            if len(results) == 0:
                more2 = False
        # logging.critical(details)
        self.response.out.write(simplejson.dumps(details))

class SMSAPI(BaseHandler):
    def post(self):
        from_number = self.request.get('from')
        message = self.request.get('message')



        logging.debug(from_number)
        subscriber = Subscriber.query(Subscriber.branch_phone == from_number).get()
        logging.debug(str(subscriber))
        if subscriber:
            company = subscriber.company.get()

            feedback = Feedback()
            feedback.branch_name = subscriber.branch_name
            feedback.branch_phone = subscriber.branch_phone
            feedback.branch_key = subscriber.key
            feedback.company_name = company.company_name
            feedback.company_key = company.key

            if '#' in message:
                reverse_msg = ""
                for char in message:
                    reverse_msg = char + reverse_msg
                reverse_id = reverse_msg[:reverse_msg.index(":")]
                message = ""
                for char in reverse_id:
                    message = char + message
                logging.critical(message)

            if '+' in message:
                reverse_msg = ""
                for char in message:
                    reverse_msg = reverse_msg + char
                reverse_id = reverse_msg[:reverse_msg.index(":")]
                logging.critical(reverse_id)
                feedback.feedback_type = 'complain'
                feedback.complain_number = reverse_id

            # feedback.feedback_details = message
            if message == '5000':
                feedback.feedback_type = message
                feedback.complain_number = None
            if message == '10000':
                feedback.feedback_type = message
                feedback.complain_number = None
            if message == '15000':
                feedback.feedback_type = message
                feedback.complain_number = None

            feedback.put()

        return

class testing(BaseHandler):
    def get(self):
        self.render('frontend/test.html')

    def post(self):
        url = "https://www.mlepay.com/api/v2/transaction/create"
        nonce = '2380951954836459' #''.join(random.choice(string.digits) for x in range(16))
        unix = '1403773939'
        logging.critical(nonce)
        date_now = datetime.datetime.now()
        #unix = int(time.mktime(date_now.timetuple()))
        expiry = date_now + datetime.timedelta(days=1)
        logging.critical(unix)
        #logging.critical(int(time.mktime(expiry.timetuple())))

        request_body = {
            "receiver_email": "faye@sym.ph",
            "sender_email": 'faye@sym.ph',
            "sender_name": 'faye mojado',
            "sender_phone": '1234567890',
            "sender_address": "Cebu City",
            "amount": 1000,
            "currency": "PHP",
            "nonce": nonce,
            "timestamp": unix,
            "expiry": 1403860339,
            "payload": '1234',
            "description": "creating safebox transaction"
        }
        mybody = simplejson.dumps(request_body)
        request_body = mybody
        base_string = "POST"
        base_string += "&" + urllib.quote(url)
        base_string += "&" + urllib.quote(mybody)
        secret_key = "Y2I0NGUyNDQtYWZlNy00MzBmLWJjMjgtMzE3NjIxZDgzMDRkZWQ4ZDdlMzItNjlkYi00NWNkLTk0OTEtMWNiMjMwZjU0OTM0"

        signature = base64.b64encode(hmac.new(secret_key, base_string, hashlib.sha256).digest()).decode()
        logging.critical(signature)
        signature1 = base64.b64encode(hmac.new(secret_key, base_string, hashlib.sha256).digest())
        logging.critical(signature1)
        logging.critical(base_string)

        headers = {"X-Signature": signature, "Content-Type":"application/json"}
        response = urlfetch.fetch(url=url, payload=request_body, method=urlfetch.POST, headers=headers)
        logging.critical(response.content)

class ErrorHandler(BaseHandler):
    def get(self, page):
        if page == 'import-feedback':
            # #*15000,6/7/14 9:50
            subscriber_key = ndb.Key(urlsafe='ag9zfnN5bXBoZmVlZGJhY2tyFwsSClN1YnNjcmliZXIYgICAgMCInQkM')
            company_key = ndb.Key(urlsafe='ag9zfnN5bXBoZmVlZGJhY2tyFAsSB0NvbXBhbnkYgICAgICAgAoM')

            feedbacks = []

            with open('feedback_import.csv','rb') as csvfile:
                line_reader = csv.reader(csvfile)


                for row in line_reader:
                    feedback = Feedback()
                    if row[0] == "#*15000":
                        feedback.feedback_type = '15000'
                        feedback.complain_number = None
                    elif row[0] == "#*10000":
                        feedback.feedback_type = '10000'
                        feedback.complain_number = None
                    elif row[0] == "#*5000":
                        feedback.feedback_type = '5000'
                        feedback.complain_number = None

                    feedback.branch_name = "Test Branch"
                    feedback.branch_phone = "Test Phone"
                    feedback.branch_key = subscriber_key
                    feedback.company_name = "M Lhuillier"
                    feedback.company_key = company_key

                    created_time = datetime.datetime.strptime(row[1],"%m/%d/%y %H:%M")
                    feedback.created = created_time
                    feedbacks.append(feedback)

            if feedbacks:
                ndb.put_multi(feedbacks)
                self.response.write('done ')

            self.response.write('imported')
            return
        self.tv["current_page"] = "ERROR"
        self.render('frontend/dynamic404.html')


site_domain = SETTINGS["site_domain"].replace(".","\.")

app = webapp2.WSGIApplication([
    routes.DomainRoute(r'<:' + site_domain + '|localhost|' + SETTINGS["app_id"] + '\.appspot\.com>', [
        webapp2.Route('/', handler=FrontPage, name="www-front"),
        webapp2.Route('/register', handler=RegisterPage, name="www-register"),
        webapp2.Route('/dashboard', handler=DashboardPage, name="www-dashboard"),
        webapp2.Route('/logout', handler=Logout, name="www-logout"),
        webapp2.Route('/login', handler=LoginPage, name="www-login"),
        webapp2.Route('/companies', handler=CompaniesPage, name="www-companies"),
        webapp2.Route(r'/edit/company/<:.*>', handler=CompanyUpdate, name="www-company-edit"),
        webapp2.Route(r'/delete/company/<:.*>', handler=CompanyDelete, name="www-company-delete"),
        webapp2.Route('/subscribers', handler=SubscribersPage, name="www-subscribers"),
        webapp2.Route(r'/edit/subscriber/<:.*>', handler=SubscriberUpdate, name="www-subscriber-edit"),
        webapp2.Route(r'/delete/subscriber/<:.*>', handler=SubscriberDelete, name="www-subscriber-delete"),
        webapp2.Route('/feedbacks', handler=FeedbacksPage, name="www-feedbacks"),
        webapp2.Route('/graphs', handler=GraphsPage, name="www-graphs"),
        webapp2.Route('/feedback/graphs', handler=FeedbacksGraphPage, name="www-feedback-graphs"),

        webapp2.Route('/api/sms', handler=SMSAPI, name="www-smsapi"),
        webapp2.Route('/testing', handler=testing, name="www-test"),

        webapp2.Route(r'/<:.*>', ErrorHandler)
    ])
])
