from google.appengine.ext import ndb
import time


class User(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    password = ndb.StringProperty()
    name = ndb.StringProperty()
    first_name = ndb.StringProperty()
    middle_name = ndb.StringProperty()
    last_name = ndb.StringProperty()
    admin = ndb.BooleanProperty(default=False)
    
    def to_object(self):
        details = {}
        details["created"] = int(time.mktime(self.created.timetuple()))
        details["updated"] = int(time.mktime(self.updated.timetuple()))
        details["name"] = self.name
        details["admin"] = self.admin
        return details



class PasswordResetToken(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    username = ndb.StringProperty()
    token = ndb.StringProperty()
    expires = ndb.DateTimeProperty()


class Company(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    company_name = ndb.StringProperty()
    company_address = ndb.StringProperty()
    company_phone = ndb.StringProperty()

    def to_object(self):
        details = {}
        details["id"] = self.key.id()
        details["key"] = self.key.urlsafe()
        details["created"] = int(time.mktime(self.created.timetuple()))
        details["updated"] = int(time.mktime(self.created.timetuple()))
        details["company_name"] = self.company_name
        details["company_address"] = self.company_address
        try:
            details["company_phone"] = self.company_phone
        except:
            details["company_phone"] = ""

        return details

class Subscriber(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    company = ndb.KeyProperty()
    branch_name = ndb.StringProperty()
    branch_address = ndb.StringProperty()
    branch_phone = ndb.StringProperty()

    def to_object(self):
        details = {}
        details["id"] = self.key.id()
        details["key"] = self.key.urlsafe()
        details["created"] = int(time.mktime(self.created.timetuple()))
        details["updated"] = int(time.mktime(self.created.timetuple()))
        details["branch_name"] = self.branch_name
        details["branch_address"] = self.branch_address
        details["company"] = self.company
        try:
            details["branch_phone"] = self.branch_phone
        except:
            details["branch_phone"] = ""

        return details

class Feedback(ndb.Model):
    created = ndb.DateTimeProperty(auto_now_add=True)
    updated = ndb.DateTimeProperty(auto_now=True)
    company_key = ndb.KeyProperty()
    company_name = ndb.StringProperty()
    branch_key = ndb.KeyProperty()
    branch_name = ndb.StringProperty()
    branch_phone = ndb.StringProperty()
    feedback_details = ndb.TextProperty()
    feedback_type = ndb.StringProperty()
    complain_number = ndb.StringProperty()

    def to_object(self):
        details = {}
        details["id"] = self.key.id()
        details["key"] = self.key.urlsafe()
        details["created"] = self.created.strftime("%b %d %Y %H:%M:%S")
        details["updated"] = self.updated.strftime("%b %d %Y %H:%M:%S")
        details["branch_name"] = self.branch_name
        details["company_name"] = self.company_name
        details["feedback_details"] = self.feedback_details
        details["feedback_type"] = self.feedback_type
        try:
            details["branch_phone"] = self.branch_phone
        except:
            details["branch_phone"] = None

        try:
            details["complain_number"] = self.complain_number
        except:
            details["complain_number"] = None

        return details

